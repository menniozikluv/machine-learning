import { createRouter, createWebHistory } from 'vue-router';
import FormulaireComponent from '@/components/FormulaireComponent.vue';
import DataCleaningComponent from '@/components/DataCleaningComponent.vue';
import dataChartComponent from '@/components/dataChartComponent.vue';

const routes = [
  { path: '/formulaire', component: FormulaireComponent },
  { path: '/data-cleaning', component: DataCleaningComponent },
  { path: '/', component: dataChartComponent }

];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
